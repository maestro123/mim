LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE                  := MIMNative-v1
LOCAL_SRC_FILES               := MIMNative/MIMNative.cpp
#LOCAL_LDLIBS                  := X:/android-ndk-r10c/platforms/android-19/arch-arm/usr/lib/liblog.so X:/android-ndk-r10c/platforms/android-19/arch-arm/usr/lib/libjnigraphics.so
#LOCAL_LDLIBS                  := F:/ndk/platforms/android-19/arch-arm/usr/lib/liblog.so F:/ndk/platforms/android-19/arch-arm/usr/lib/libjnigraphics.so
LOCAL_LDLIBS                  := /home/artyom/ndk/platforms/android-19/arch-arm/usr/lib/liblog.so /home/artyom/ndk/platforms/android-19/arch-arm/usr/lib/libjnigraphics.so
LOCAL_LDLIBS                  += -lz
LOCAL_CPPFLAGS += -fexceptions

include $(BUILD_SHARED_LIBRARY)