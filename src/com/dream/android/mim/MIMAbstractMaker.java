package com.dream.android.mim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public abstract class MIMAbstractMaker {

    public abstract Bitmap getBitmap(ImageLoadObject loadObject, Context context);

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return Math.max(width > 1920 || height > 1920 ? 3 : 1, inSampleSize);
    }

}
