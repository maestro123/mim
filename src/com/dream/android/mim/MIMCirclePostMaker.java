package com.dream.android.mim;

import android.graphics.*;

/**
 * Created by artyom on 8/4/14.
 */
public class MIMCirclePostMaker implements MIMAbstractPostMaker {

    public static final String TAG = MIMCirclePostMaker.class.getSimpleName();

    private Paint mStrokePaint;

    public MIMCirclePostMaker(int strokeWidth, String strokeColor) {
        mStrokePaint = new Paint();
        mStrokePaint.setAntiAlias(true);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeWidth(strokeWidth);
        mStrokePaint.setColor(Color.parseColor(strokeColor));
    }

    public MIMCirclePostMaker() {
    }

    @Override
    public Bitmap processBitmap(ImageLoadObject imageLoadObject, Bitmap bitmap) {
        int width = imageLoadObject.getWidth();
        int height = imageLoadObject.getHeight();
        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();

        final Rect srcRect = MIMUtils.calculateSrcRect(bitmap.getWidth(), bitmap.getHeight(), width, height);
        final Rect dstRect = new Rect(0, 0, width, height);

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(width / 2, height / 2, width / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        paint.setColor(Color.RED);
        canvas.drawCircle(width/2, height/2, width/2, paint);
        canvas.drawBitmap(bitmap, srcRect, dstRect, paint);
        if (mStrokePaint != null)
            canvas.drawCircle(width / 2, height / 2, width / 2 - mStrokePaint.getStrokeWidth() / 2, mStrokePaint);
        bitmap.recycle();
        return output;
    }
}
