package com.dream.android.mim;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by Artyom on 1/20/2015.
 */
public interface IMImageView {

    void setImageDrawable(Drawable drawable);

    void setImageBitmap(Bitmap bitmap);

    void setImageResource(int resource);

}
