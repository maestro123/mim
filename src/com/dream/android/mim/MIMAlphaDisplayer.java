package com.dream.android.mim;

import android.animation.ObjectAnimator;
import android.view.View;

public class MIMAlphaDisplayer implements MIMAbstractDisplayer {

    private int duration = 300;

    public MIMAlphaDisplayer() {
    }

    public MIMAlphaDisplayer(int duration) {
        this.duration = duration;
    }

    @Override
    public void display(View img) {
        ObjectAnimator a = ObjectAnimator.ofFloat(img, "alpha", 0f, 1f);
        a.setDuration(duration);
        a.start();
    }

}