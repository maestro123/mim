package com.dream.android.mim;

import android.content.res.Resources;
import android.graphics.*;
import android.util.Log;

/**
 * Created by Artyom on 3/8/2015.
 */
public class MRoundedDrawable extends RecyclingBitmapDrawable {

    public static final String TAG = MRoundedDrawable.class.getSimpleName();

    private float mRadius;

    public MRoundedDrawable(Resources res, Bitmap bitmap, float radius) {
        super(res, bitmap);
        mRadius = radius;
    }

    @Override
    public void draw(Canvas canvas) {
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = getBounds();
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, mRadius, mRadius, paint);// draw round
        // 4Corner

//            if (!roundTL) {
//                Rect rectTL = new Rect(0, 0, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
//                canvas.drawRect(rectTL, paint);
//            }
//            if (!roundTR) {
//                Rect rectTR = new Rect(bitmap.getWidth() / 2, 0, bitmap.getWidth(), bitmap.getHeight() / 2);
//                canvas.drawRect(rectTR, paint);
//            }
//            if (!roundBR) {
        Rect rectBR = new Rect(rect.width() / 2, rect.height() / 2, rect.width(), rect.height());
        canvas.drawRect(rectBR, paint);
//            }
//            if (!roundBL) {
        Rect rectBL = new Rect(0, rect.height() / 2, rect.width() / 2, rect.height());
        canvas.drawRect(rectBL, paint);
//            }

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(getBitmap(), rect, rect, paint);

//        super.draw(canvas);

    }
}
