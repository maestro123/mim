package com.dream.android.mim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class MIMDefaultMaker extends MIMAbstractMaker {

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(loadObject.getPath(), opt);
            opt.inSampleSize = calculateInSampleSize(opt, loadObject.getWidth(), loadObject.getHeight());
            opt.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(loadObject.getPath(), opt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
