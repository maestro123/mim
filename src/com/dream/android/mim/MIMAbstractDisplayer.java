package com.dream.android.mim;

import android.view.View;

public interface MIMAbstractDisplayer {

    void display(View img);

}
