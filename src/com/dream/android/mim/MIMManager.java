package com.dream.android.mim;

import android.util.Log;

import java.util.HashMap;

/**
 * Created by Artyom on 28.04.2014.
 */
public class MIMManager {

    public static final String TAG = MIMManager.class.getSimpleName();
    private final int MAX_CACHES = 8;

    private static volatile MIMManager instance;
    private HashMap<Object, MIM> mimArray = new HashMap<Object, MIM>(5);
    private HashMap<Object, MIMCache> mimCaches = new HashMap<Object, MIMCache>(8);

    MIMManager() {
    }

    public static MIMManager getInstance() {
        MIMManager localInstance = instance;
        if (localInstance == null) {
            synchronized (MIMManager.class) {
                localInstance = instance;
                if (localInstance == null)
                    localInstance = instance = new MIMManager();
            }
        }
        return localInstance;
    }

    public MIM getMIM(Object key) {
        return mimArray.get(key);
    }

    public void addMIM(Object key, MIM mim) {
        if (mimArray.containsKey(key)) {
            Log.e(TAG, "Already have MIM.class for key " + key);
        } else {
            mimArray.put(key, mim);
        }
    }

    public void removeMIM(Object key) {
        if (!mimArray.containsKey(key)) {
            Log.e(TAG, "No MIM.class found for key " + key);
        } else
            mimArray.remove(key);
    }

    public void addMIMCache(Object key, MIMCache cache) {
        if (mimCaches.size() == MAX_CACHES) {
            Log.e(TAG, "Maximum MIMCache.class that can stored is " + MAX_CACHES);
        } else if (mimCaches.containsKey(key)) {
            Log.e(TAG, "Already have MIMCache.class for key " + key);
        } else {
            mimCaches.put(key, cache);
        }
    }

    public MIMCache getCache(Object key) {
        return mimCaches.get(key);
    }

    public void removeCache(Object key) {
        MIMCache cache = mimCaches.get(key);
        if (cache != null) {
            cache.clear();
            mimCaches.remove(key);
        }
    }

}
