package com.dream.android.mim;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONObject;

import android.util.Log;

public class MIMSearchEngine {

    public static final String imageApi = "https://ajax.googleapis.com/ajax/services/search/images?";
    public static final String PARAM_RESPONSE_DATA = "responseData";
    public static final String PARAM_RESULT = "results";
    public static final String PARAM_IMAGE_URL = "url";

    public static String search(String searchWord) {
        String bestMatch = null;

        try {
            URL url = new URL(imageApi + "v=1.0&q="
                    + URLEncoder.encode(searchWord, "UTF-8") + "&rsz=1");// +
            // "&imgsz=large"
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(10000);

            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            Log.e("search result = ", "search result = " + builder.toString());

            JSONObject j = new JSONObject(builder.toString());

            if (j != null) {
                bestMatch = j.getJSONObject(PARAM_RESPONSE_DATA)
                        .getJSONArray(PARAM_RESULT).getJSONObject(0)
                        .getString(PARAM_IMAGE_URL);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("best match = ", "best match = " + bestMatch);
        return bestMatch;
    }

}
