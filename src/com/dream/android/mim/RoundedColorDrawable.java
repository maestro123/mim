package com.dream.android.mim;

import android.graphics.*;
import android.graphics.drawable.ColorDrawable;

/**
 * Created by artyom on 8/14/14.
 */
public class RoundedColorDrawable extends ColorDrawable {

    private final float mCornerRadius;
    private final Paint mPaint;
    private Paint mStrokePaint;
    private RectF mRect = new RectF();

    public RoundedColorDrawable(int color, float cornerRadius) {
        mCornerRadius = cornerRadius;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(color);
    }

    public RoundedColorDrawable setStrokeWidth(float width) {
        getStrokePaint().setStrokeWidth(width);
        return this;
    }

    public RoundedColorDrawable setStrokeColor(int color) {
        getStrokePaint().setColor(color);
        return this;
    }

    private final Paint getStrokePaint() {
        if (mStrokePaint == null) {
            mStrokePaint = new Paint(mPaint);
            mStrokePaint.setStyle(Paint.Style.STROKE);
            mStrokePaint.setStrokeJoin(Paint.Join.ROUND);
        }
        return mStrokePaint;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        mRect = new RectF(bounds);
    }

    @Override
    public void draw(Canvas canvas) {
        mRect = new RectF(getBounds());
        canvas.drawRoundRect(mRect, mCornerRadius, mCornerRadius, mPaint);
        if (mStrokePaint != null)
            canvas.drawRoundRect(mRect, mCornerRadius, mCornerRadius, mStrokePaint);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
        if (mStrokePaint != null)
            mStrokePaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
        if (mStrokePaint != null)
            mStrokePaint.setColorFilter(cf);
    }

}
