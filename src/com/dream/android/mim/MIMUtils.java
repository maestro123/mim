package com.dream.android.mim;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Created by artyom on 8/17/14.
 */
public class MIMUtils {

    public static final String OKEY_LANDSCAPE = "land";
    public static final String OKEY_PORTRAIT = "port";

    private static final String OKEY_DIVIDER = "_";

    public static boolean isColorDark(int color) {
        return 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255 > 0.5;
    }

    public static double getColorLightness(int color) {
        return 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
    }

    public static int blendColors(int color1, int color2, float ratio) {
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    public static int adjustAlpha(int color, int alpha) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public static int mixColors(int col1, int col2) {
        int a1, r1, g1, b1, a2, r2, g2, b2;

        a1 = Color.alpha(col1);
        r1 = Color.red(col1);
        g1 = Color.green(col1);
        b1 = Color.blue(col1);

        a2 = Color.alpha(col2);
        r2 = Color.red(col2);
        g2 = Color.green(col2);
        b2 = Color.blue(col2);

        int a3 = (a1 + a2) / 2;
        int r3 = (r1 + r2) / 2;
        int g3 = (g1 + g2) / 2;
        int b3 = (b1 + b2) / 2;

        return Color.argb(a3, r3, g3, b3);
    }

    public static String makePathFromUrl(String _s) {
        String string = _s.replaceAll("[\\\\s.:;&=<>/]", "");
        if (string.length() > 160) {
            string = String.valueOf(string.hashCode());
        }
        return string;
    }

    public static void setImageByViewType(Object imageView, Bitmap bitmap) {
        if (imageView != null)
            if (imageView instanceof ImageView) {
                ((ImageView) imageView).setImageBitmap(bitmap);
            } else if (imageView instanceof IMImageView) {
                ((IMImageView) imageView).setImageBitmap(bitmap);
            } else {
                throw new UnsupportedOperationException("unsupported class...");
            }
    }

    public static void setImageByViewType(Object imageView, Drawable drawable) {
        if (imageView != null)
            if (imageView instanceof ImageView) {
                ((ImageView) imageView).setImageDrawable(drawable);
            } else if (imageView instanceof IMImageView) {
                ((IMImageView) imageView).setImageDrawable(drawable);
            } else {
                throw new UnsupportedOperationException("unsupported class...");
            }
    }

    public static void setImageByViewType(Object imageView, int id) {
        if (imageView != null)
            if (imageView instanceof ImageView) {
                ((ImageView) imageView).setImageResource(id);
            } else if (imageView instanceof IMImageView) {
                ((IMImageView) imageView).setImageResource(id);
            } else {
                throw new UnsupportedOperationException("unsupported class...");
            }
    }

    public static Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight) {
        final float srcAspect = (float) srcWidth / (float) srcHeight;
        final float dstAspect = (float) dstWidth / (float) dstHeight;

        if (srcAspect > dstAspect) {
            final int srcRectWidth = (int) (srcHeight * dstAspect);
            final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
            return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth,
                    srcHeight);
        } else {
            final int srcRectHeight = (int) (srcWidth / dstAspect);
            final int scrRectTop = (srcHeight - srcRectHeight) / 2;
            return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
        }
    }

    public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight) {
        Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight);
        Rect dstRect = new Rect(0, 0, dstWidth, dstHeight);
        Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));
        return scaledBitmap;
    }

    public static String makeOKey(Context context, String key, String... params) {
        StringBuilder builder = new StringBuilder(key).append(OKEY_DIVIDER);
        builder.append(context.getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE ? OKEY_LANDSCAPE : OKEY_PORTRAIT);
        if (params != null && params.length > 0) {
            for (String str : params) {
                builder.append(OKEY_DIVIDER)
                        .append(str);
            }
        }
        return builder.toString();
    }

}
