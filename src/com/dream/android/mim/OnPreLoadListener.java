package com.dream.android.mim;

/**
 * Created by Artyom on 9/1/2015.
 */
public interface OnPreLoadListener {

    public void onPreLoad(ImageLoadObject loadObject);

}
