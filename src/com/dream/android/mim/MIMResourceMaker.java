package com.dream.android.mim;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class MIMResourceMaker extends MIMDefaultMaker {

    public static final String TAG = MIMResourceMaker.class.getSimpleName();

    public MIMResourceMaker() {
    }

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        Resources resources = ctx.getResources();
        int drawableId = Integer.parseInt(loadObject.getPath());
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = loadObject.getConfig();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, drawableId, opt);
        opt.inSampleSize = calculateInSampleSize(opt, loadObject.getWidth(), loadObject.getHeight());
        opt.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(resources, drawableId, opt);
    }

}
